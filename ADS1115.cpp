//
// Created by prive on 1/19/20.
//

#include "ADS1115.h"

ADS1115::ADS1115(uint8_t address) {
    this->address = address;
}

void ADS1115::loadConfig() {
    Wire.beginTransmission(address);
    Wire.write(ADS1115_CONFIG_REGISTER);
    Wire.write(config >> 8U);
    Wire.write(config);
    Wire.endTransmission();
}

void ADS1115::setup() {
    config = 0;
    config |= (uint16_t) ADS1115_INPUT_CHANNEL_OFFSET << ADS1115_CONFIG_MUX;        // AIN_P = AIN0 and AIN_N = GND
    config |= (uint16_t) 0b000 << ADS1115_CONFIG_PGA;       // FS = ±6.144 V
    config |= (uint16_t) ADS1115_CONFIG_MODE_CONTINUOUS << ADS1115_CONFIG_MODE;
    config |= (uint16_t) ADS1115_CONFIG_DR_860SPS << ADS1115_CONFIG_DR;
    config |= (uint16_t) 0b0 << ADS1115_CONFIG_COMP_MODE;
    config |= (uint16_t) 0b0 << ADS1115_CONFIG_COMP_POL;
    config |= (uint16_t) 0b0 << ADS1115_CONFIG_COMP_LAT;
    config |= (uint16_t) 0b11 << ADS1115_CONFIG_COMP_QUE;   // Disable comparator

    loadConfig();
}

void ADS1115::selectChannel(uint8_t channel) {
    if (channel > 3) {
        channel = 0;
    }

    channel += ADS1115_INPUT_CHANNEL_OFFSET;
    config &= ~((uint16_t) 0b111 << ADS1115_CONFIG_MUX);
    config |= (uint16_t) channel << ADS1115_CONFIG_MUX;
    loadConfig();
}

uint16_t ADS1115::readCurrentChannel() {
    // Set pointer register to Conversion register
    Wire.beginTransmission(address);
    Wire.write(ADS1115_CONVERSION_REGISTER);
    Wire.endTransmission();

    // Read from ADC
    Wire.requestFrom(address, 2);
    uint16_t reading = (uint16_t) Wire.read() << 8U;
    reading |= (uint8_t) Wire.read();

    if (reading >> 15U) {    // Check if reading is overflowing
        // Invalid reading
        return 0;
    }

    return reading;
}

uint16_t ADS1115::readChannel(uint8_t channel) {
    selectChannel(channel);
    waitForConversion();
    return readCurrentChannel();
}
