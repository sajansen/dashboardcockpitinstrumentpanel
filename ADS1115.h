//
// Created by prive on 1/19/20.
//

#ifndef DASHBOARDCOCKPITINSTRUMENTPANEL_ADS1115_H
#define DASHBOARDCOCKPITINSTRUMENTPANEL_ADS1115_H

#include <Arduino.h>
#include <Wire.h>

// Registers
#define ADS1115_CONVERSION_REGISTER 0x00
#define ADS1115_CONFIG_REGISTER 0x01

// Config bit mapping
#define ADS1115_CONFIG_OS 15U
#define ADS1115_CONFIG_MUX 12U
#define ADS1115_CONFIG_PGA 9U
#define ADS1115_CONFIG_MODE 8U
#define ADS1115_CONFIG_DR 5U
#define ADS1115_CONFIG_COMP_MODE 4U
#define ADS1115_CONFIG_COMP_POL 3U
#define ADS1115_CONFIG_COMP_LAT 2U
#define ADS1115_CONFIG_COMP_QUE 0U

#define ADS1115_CONFIG_MODE_CONTINUOUS  0x00U
#define ADS1115_CONFIG_MODE_SINGLE_SHOT 0x01U

#define ADS1115_CONFIG_DR_8SPS      0x00
#define ADS1115_CONFIG_DR_16SPS     0x01
#define ADS1115_CONFIG_DR_32SPS     0x02
#define ADS1115_CONFIG_DR_64SPS     0x03
#define ADS1115_CONFIG_DR_128SPS    0x04
#define ADS1115_CONFIG_DR_250SPS    0x05
#define ADS1115_CONFIG_DR_475SPS    0x06
#define ADS1115_CONFIG_DR_860SPS    0x07

// Input selection
#define ADS1115_CHANNEL_0 0b0000100
#define ADS1115_CHANNEL_1 0b0000101
#define ADS1115_CHANNEL_2 0b0000110
#define ADS1115_CHANNEL_3 0b0000111
#define ADS1115_INPUT_CHANNEL_OFFSET ADS1115_CHANNEL_0

#define ADS1115_CONVERSION_DURATION 2   // in milliseconds

class ADS1115 {
public:
    ADS1115(uint8_t address);
    void loadConfig();
    void setup();
    void selectChannel(uint8_t channel);
    uint16_t readCurrentChannel();
    uint16_t readChannel(uint8_t channel);
    void waitForConversion() {
        delay(ADS1115_CONVERSION_DURATION);
    };

private:
    uint8_t address;
    uint16_t config = 0x00;
};


#endif //DASHBOARDCOCKPITINSTRUMENTPANEL_ADS1115_H
