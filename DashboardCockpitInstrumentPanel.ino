#include <Arduino.h>
#include <SPI.h>
#include "settings.h"
#include "utils.h"
#include "FeedbackMotor.h"
#include "outputs.h"
#include "identifiers.h"
#include "screen.h"
#include "inputs.h"


RotaryEncoder rotaryEncoder;

struct SimulatorData simulatorData = {0};
uint8_t OPERATION_MODE = OPERATION_MODE_UNCONNECTED;

void setupSPI();

void handleNextOutputCycle();

void handleSimulatorDataUpdate();

uint8_t updateSimulatorData(SimulatorData *simulatorDataObj);

void processSimulatorData(SimulatorData *simulatorData);

void setup() {
    setupScreen();
    setupSPI();
    setupOutputs();
    setupInputs();
    Serial.begin(BAUD_RATE);

    showDashboard();
}

void loop() {
#if DEMO_MODE
    showSwitchStatus("Landing\nGear", false);
    delay(1500);
    showSwitchStatus("Landing\nGear", true);
    delay(1500);
    showSwitchStatus("Landing\nGear", false);
    delay(1500);

    showSliderStatus("Rudder", -100);
    delay(500);
    for (int16_t i = -100; i < 101; i += 3) {
        showSliderStatus("Rudder", i);
    }
    showSliderStatus("Rudder", 100);
    delay(1500);

    showSpeedStatus("Ground\nSpeed", 0);
    delay(500);
    for (int16_t x = -100; x < 555; x += 19) {
        showSpeedStatus("Ground\nSpeed", x);
    }
    showSpeedStatus("Ground\nSpeed", 357);
    delay(1500);

    showAltitudeStatus("Altitude", -10);
    delay(500);
    for (int16_t h = 0; h < 15000; h += 19 + h / 2) {
        showAltitudeStatus("Altitude", h);
    }
    showAltitudeStatus("Altitude", 13570);
    delay(1500);
    return;
#endif

    handleInputs();
    handleSimulatorDataUpdate();
    handleNextOutputCycle();
}

void handleSimulatorDataUpdate() {
    static unsigned long lastSimulatorDataUpdateTime = 0;
    static uint8_t errorCount = 0;
    static uint8_t connectionErrorCount = 0;
    if (lastSimulatorDataUpdateTime + SIMULATOR_DATA_UPDATE_INTERVAL > millis()) {
        return;
    }

    showDataUpdateInProgress(true);

    uint8_t updateResultErrorCode = updateSimulatorData(&simulatorData);

    lastSimulatorDataUpdateTime = millis();
    if (updateResultErrorCode == 1) {
        showDataUpdateInProgress(false);
        errorCount++;

        if (errorCount > 5) {
            showWarningMessage(memStr(F("To many\nserial\nfailures")));
        }

        return;
    } else if (updateResultErrorCode == 2) {
        showDataUpdateInProgress(false);
        connectionErrorCount++;

        if (connectionErrorCount > 3) {
            OPERATION_MODE = OPERATION_MODE_UNCONNECTED;
            turnOffFeedbackMotor();

            if (connectionErrorCount >= 30) {
                showBlackScreen(memStr(F("Not connected")));
                connectionErrorCount = 30;
            } else {
                showErrorMessage(memStr(F("No serial\nconnection")));
            }
        }

        return;
    }

    errorCount = 0;
    connectionErrorCount = 0;

    processSimulatorData(&simulatorData);
    showDataUpdateInProgress(false);
}

/**
 *
 * @param simulatorDataObj
 * @return error_message:
 *  0: it's fine
 *  1: received size is incorrect
 *  2: no serial connection
 */
uint8_t updateSimulatorData(SimulatorData *simulatorDataObj) {
#if DEMO_SERIAL_CONNECTION
    return 0;       // Zolang er geen PC verbonden s, geeft het onderstaande alleen maar timeouts..
#endif

    serialFlush();

    // Request for an update
    sendMeta(0, SERIAL_DATA_TYPE_UPDATE_REQUEST, 0);

    // Retrieve the update
    size_t bytes_read = Serial.readBytes((char *) simulatorDataObj, sizeof(SimulatorData));

    if (bytes_read == 0) {
        return 2;
    }

    return bytes_read != sizeof(SimulatorData) ? 1 : 0;
}

void setupSPI() {
    SPI.beginTransaction(SPISettings(
            1000000, /* 4 MHz */
            MSBFIRST,
            SPI_MODE0
    ));
    SPI.begin();
}

void processSimulatorData(SimulatorData *simulatorData) {
    OPERATION_MODE = simulatorData->OPERATION_MODE;

    handleLanding(simulatorData);
    handleCrash(simulatorData);
    handleOverspeed(simulatorData);
//    handleTurbulence(simulatorData);

    handleLandingGearUpdate(simulatorData);
    handleFlapsPosition(simulatorData);
    handleCustomButtons(simulatorData);
    handleBacklight(simulatorData);

    rotaryEncoder.autoPilotAvailable = simulatorData->AUTO_PILOT_AVAILABLE;

    if (simulatorData->PAUSE_FLAG) {
        turnOffFeedbackMotor();
        showWarningMessage(memStr(F("Paused")));
    }
}

void handleNextOutputCycle() {
    handleScreenCycle();
    nextCycleFeedbackMotor();
    handleLandingGearCycle();
    handleOutputBoardsCycle();
}