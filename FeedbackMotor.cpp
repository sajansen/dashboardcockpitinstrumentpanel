//
// Created by Samuel on 26-8-2019.
//

#include "FeedbackMotor.h"

bool isFeedbackMotorRunning = false;
uint8_t feedbackMotorForce = 255;
unsigned long feedbackMotorStopTime = 0;

void setupFeedBackMotor() {
    pinMode(FEEDBACK_MOTOR_PIN, OUTPUT);
    digitalWrite(FEEDBACK_MOTOR_PIN, LOW);
}

void startFeedbackMotor(unsigned long duration) {
    startFeedbackMotor(duration, 255);
}

void startFeedbackMotor(unsigned long duration, uint8_t force) {
    feedbackMotorForce = force;
    feedbackMotorStopTime = millis() + duration;
    turnOnFeedbackMotor();
}

void turnOnFeedbackMotor() {
    if (!inputMatrix.getNewValueForIdentifier(CUSTOM_G_ID)) {
        return;
    }
    analogWrite(FEEDBACK_MOTOR_PIN, feedbackMotorForce);
    isFeedbackMotorRunning = true;
}

void turnOffFeedbackMotor() {
    isFeedbackMotorRunning = false;
    digitalWrite(FEEDBACK_MOTOR_PIN, LOW);
}

void nextCycleFeedbackMotor() {
    if (!isFeedbackMotorRunning) return;

    if (!inputMatrix.getNewValueForIdentifier(CUSTOM_G_ID) || millis() > feedbackMotorStopTime) {
        turnOffFeedbackMotor();
    }
}