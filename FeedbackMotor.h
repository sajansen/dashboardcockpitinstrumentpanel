//
// Created by Samuel on 26-8-2019.
//

#ifndef SERIALTOKEYBOARDSTROKESDEVICE_FEEDBACKMOTOR_H
#define SERIALTOKEYBOARDSTROKESDEVICE_FEEDBACKMOTOR_H

#include <Arduino.h>
#include "settings.h"
#include "InputMatrix.h"

#ifndef FEEDBACK_MOTOR_PIN      // Must be defined in settings.h
#define FEEDBACK_MOTOR_PIN 9
#endif

void setupFeedBackMotor();
void startFeedbackMotor(unsigned long duration);
void startFeedbackMotor(unsigned long duration, uint8_t force);
void turnOnFeedbackMotor();
void turnOffFeedbackMotor();
void nextCycleFeedbackMotor();

#endif //SERIALTOKEYBOARDSTROKESDEVICE_FEEDBACKMOTOR_H
