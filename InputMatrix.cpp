//
// Created by Samuel on 26-8-2019.
//

#include "InputMatrix.h"

void InputMatrix::setup() {
    pinMode(INPUT_MATRIX_LOAD_PIN, OUTPUT);
    digitalWrite(INPUT_MATRIX_LOAD_PIN, HIGH);
}

void InputMatrix::readAll() {
    updateCurrentValuesWithNewValues();

    SPI.transfer(1);
    for (uint8_t i = 0; i < 8; i++) {
        digitalPulse(INPUT_MATRIX_LOAD_PIN, LOW);   // Load new +5V output values
        digitalPulse(INPUT_MATRIX_LOAD_PIN, LOW);   // Load the corresponding button input values
        read_values[i] = SPI.transfer(1U << (i + 1U));        // Load current Vss data and also prepare next Vdd data
    }
    digitalPulse(INPUT_MATRIX_LOAD_PIN, LOW);
}

bool InputMatrix::hasStateChanged() {
    for (uint8_t i = 0; i < 8; i++) {
        if (hasRowChanged(i)) {
            return true;
        }
    }
    return false;
}

bool InputMatrix::hasRowChanged(uint8_t row_index) {
    return current_values[row_index] != read_values[row_index];
}

bool InputMatrix::hasValueChanged(uint8_t row_index, uint8_t column_index) {
    return (current_values[row_index] & (1U << column_index)) != (read_values[row_index] & (1U << column_index));
}

uint8_t InputMatrix::getNewValue(uint8_t row_index, uint8_t column_index) {
    return 1U & (read_values[row_index] >> column_index);
}

uint8_t InputMatrix::getNewValueForIdentifier(uint8_t identifier) {
    for (uint8_t row = 0; row < 8; row++) {
        for (uint8_t col = 0; col < 8; col++) {
            if (inputIdentifier[row][col] != identifier) continue;

            return getNewValue(row, col);
        }
    }
    return 0;
}

uint8_t InputMatrix::getNewRowValue(const uint8_t row_index) {
    return read_values[row_index];
}

void InputMatrix::updateCurrentValuesWithNewValues() {
    for (uint8_t row = 0; row < 8; row++) {
        current_values[row] = read_values[row];
    }
}


/**
 * Handles an internal command. Returns true if command handling is done and no further actions are required.
 * Returns false for further handling of the command.
 * @param identifier
 * @return bool
 */
bool InputMatrix::handleInternalCommands(uint8_t identifier, uint8_t value) {
    if (identifier == ROTARY_ENCODER_MODE_UP) {
        rotaryEncoder.increaseMode();
        return true;
    }
    if (identifier == ROTARY_ENCODER_MODE_DOWN) {
        rotaryEncoder.decreaseMode();
        return true;
    }

#if DEMO_SERIAL_CONNECTION
    if (identifier == CUSTOM_G_ID) {
        if (value > 0) {
            startFeedbackMotor(20000);
        } else {
            turnOffFeedbackMotor();
        }
        return true;
    }
#endif

    /*
     * Add here more internal commands for buttons. Return with true.
     */


    return false;
}

void InputMatrix::sendValueUpdate(uint8_t row, uint8_t column) {
    uint8_t identifier = inputIdentifier[row][column];
    bool isSwitch = InputMatrix::isSwitch(identifier);
    uint8_t value = getNewValue(row, column);

    if (!isSwitch) {
        // Skip update if the input was released to avoid toggling on and directly off
        if (!value) {
            return;
        }
    }

    if (handleInternalCommands(identifier, value)) {
        return;
    }

    if (isSwitch) {
        sendSwitchValueUpdate(identifier, value);
    } else {
        sendToggleValueUpdate(identifier);
    }
}

bool InputMatrix::isSwitch(uint8_t identifier) {
    for (uint8_t switchId : inputMatrixSwitches) {
        if (identifier == switchId) {
            return true;
        }
    }
    return false;
}
