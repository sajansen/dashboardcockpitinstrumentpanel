//
// Created by Samuel on 26-8-2019.
//

#ifndef SERIALTOKEYBOARDSTROKESDEVICE_INPUTMATRIX_H
#define SERIALTOKEYBOARDSTROKESDEVICE_INPUTMATRIX_H

#include <Arduino.h>
#include <SPI.h>
#include "settings.h"
#include "identifiers.h"
#include "screen.h"
#include "utils.h"
#include "RotaryEncoder.h"
#include "FeedbackMotor.h"

#ifndef INPUT_MATRIX_LOAD_PIN   // Must be defined in settings.h
#define INPUT_MATRIX_LOAD_PIN 10
#endif

class InputMatrix {
  private:
    uint8_t* current_values;
    uint8_t* read_values;

    void updateCurrentValuesWithNewValues();

  public:
    InputMatrix() {
      current_values = new uint8_t[8] {0};
      read_values = new uint8_t[8] {0};
    }
    ~InputMatrix() {
      delete read_values;
    }
    void setup();
    void readAll();
    bool hasStateChanged();
    bool hasRowChanged(uint8_t row_index);
    bool hasValueChanged(uint8_t row_index, uint8_t column_index);
    uint8_t getNewValue(uint8_t row_index, uint8_t column_index);
    uint8_t getNewValueForIdentifier(uint8_t identifier);
    uint8_t getNewRowValue(uint8_t row_index);

    void sendValueUpdate(uint8_t row, uint8_t column);

    bool handleInternalCommands(uint8_t identifier, uint8_t value);

    static bool isSwitch(uint8_t identifier);
};

extern InputMatrix inputMatrix;

#endif //SERIALTOKEYBOARDSTROKESDEVICE_INPUTMATRIX_H
