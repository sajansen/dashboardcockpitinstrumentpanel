# DashboardCockpitInstrumentPanel

_A multi input/output dashboard as an extension for your game platform_

This project basically exists of a bunch of input (buttons, sliders, ...) and outputs (LED's, motors, display, ...). All are controlled by a single Arduino. This Arduino communicates with the computer to send and receive data. In this way, you have maximum control over all the in- and outputs and what they must do. 

The focus for this project is Flight Simulator X.

### Setup

#### Hardware

##### Requirements

* Arduino
* OLED display (for visual feedback about the status of your control)
* My 'Input Panel' PCB
* My 'Output Panel' PCB's
* My 'Motor Panel' PCB
* The components for these PCB's:
  * 3x 74HC595 parallel output shift register (for Input Panel and Output Panel)
  * 1x 74HC165 parallel input shift register (for Input Panel)
  * 1x transistor (for Motor Panel)
  * 1x diode (for Motor Panel)
  * Bunch of resistors
  * Bunch of pin headers
  * 4x decoupling capacitors if you are worried about the shift register chips

##### OLED Display

Connect the I2C OLED display -> Arduino:

| OLED |     | Arduino  |
| ---- | --- | -------- |
| VCC  | ->  | +5V      |
| GND  | ->  | GND      |
| SCL  | ->  | A5 (SCL) |
| SDA  | ->  | A4 (SCA) |

Note that the I2C pins (SCL and SCA) may differ per Arduino.

##### Input Panel PCB

This panel uses a SPI header. You can map each pin from this header to the corresponding pins on the Arduino. The LOAD pin must be mapped to a digital output pin. 

##### Output Panel PCB

This panel uses a SPI header. You can map each pin from this header to the corresponding pins on the Arduino. The LOAD pin must be mapped to a digital output pin. 

When connecting multiple output panels, you can chain these panels by passing each panel's MISO to the next the next panel, and return the MOSI. This is already done in the PCB, so all you need to do when linking these panels, is leaving the MISO pin unconnected on every panels master SPI header (thus not the LINK header). On the last panel in the chain, the MISO must be connected (it will be connected to the previous panel in the cain). In this way, the data from the Arduino (master) will be sent via SPI through all the panels in series. Each panel will pass on the data to the next linked panel as the SPI clock ticks.

##### Motor Panel PCB

Connect the data pin to a digital PWM output pin on the Arduino.


Note that all the custom pins, like LOAD and DATA, can be assigned in [settings.h](settings.h).


#### Software

Import these libraries from [lib/](lib/) into the Arduino Library Manager:
- [Adafruit_SH1106](https://github.com/wonho-maker/Adafruit_SH1106)

Add the path to the imported Arduino library to the CMakeLists.txt:
```
include_directories(/home/USER/Arduino/libraries/Adafruit_SH1106)
```

##### Settings

Settings are found in [settings.h](settings.h). Here, pins and intervals can be changed. Also the receiving FlightSimulator data is defined here.

##### Mappings

In [identifiers.h](identifiers.h), the identifiers for each control are defined. Also the button -> identifier mapping for the button matrix is done here.

In [identifiers.cpp](identifiers.cpp) the mapping from identifier to a string representation is done and can be configured. This string presentation is used for the OLED display only. 



