//
// Created by prive on 12/7/19.
//

#include "RotaryEncoder.h"

void rotaryEncoderInputChange() {
    volatile static uint8_t rotationCycle = 0;
    volatile static bool initialPinAReading = true;
    volatile static bool initialPinBReading = true;

    bool pinAReading = digitalRead(ROTARY_ENCODER_PIN_A);
    bool pinBReading = digitalRead(ROTARY_ENCODER_PIN_B);

    // Reset everything for a clean new start
    if (pinAReading && pinBReading) {
        rotationCycle = 0;
        initialPinAReading = true;
        initialPinBReading = true;
        return;
    }
    // Don't go into the cycles if the last cycle is finished, but there isn't a clean new start yet (cycle=0)
    if (rotationCycle == 3) {
        return;
    }

    // Init new rotation, only when there is a fresh new start
    if (rotationCycle == 0 &&
        pinAReading != pinBReading &&
        initialPinAReading && initialPinBReading) {

        rotationCycle = 1;
        initialPinAReading = pinAReading;
        initialPinBReading = pinBReading;
        return;
    }
    // Don't go into the next cycles if the rotation hasn't been initialized (cycle=1)
    if (rotationCycle == 0) {
        return;
    }

    // Check if we pass the half way rotation mark
    if (!pinAReading && !pinBReading) {
        rotationCycle = 2;
        return;
    }
    // Don't go into the next cycles if we haven't passed the half way point yet (cycle=2)
    if (rotationCycle != 2) {
        return;
    }

    // If the knob is turned back, go to the first part of the rotation: the initialization (cycle=1)
    if (pinAReading == initialPinAReading && pinBReading == initialPinBReading) {
        rotationCycle = 1;
        return;
    }

    // Ok, we are through a full rotation
    rotationCycle = 3;

    rotaryEncoder.rotaryEncoderTurns += pinAReading ? 1 : -1;
}


void RotaryEncoder::setup() {
    pinMode(ROTARY_ENCODER_PIN_A, INPUT_PULLUP);
    pinMode(ROTARY_ENCODER_PIN_B, INPUT_PULLUP);

    attachInterrupt(digitalPinToInterrupt(ROTARY_ENCODER_PIN_A), rotaryEncoderInputChange, CHANGE);
    attachInterrupt(digitalPinToInterrupt(ROTARY_ENCODER_PIN_B), rotaryEncoderInputChange, CHANGE);
}

void RotaryEncoder::clearRotations() {
    rotaryEncoderTurns = 0;
}

int8_t RotaryEncoder::getRotations() {
    if (rotaryEncoderTurns == 0) {
        return 0;
    }

    int8_t rotations = rotaryEncoderTurns;
    clearRotations();
    return rotations;
}

void RotaryEncoder::increaseMode() {
    currentMode++;
    if (currentMode > 7) {
        currentMode = 0;
    }

    if (!autoPilotAvailable) {
        if (currentMode > 2 && currentMode < 7) {
            currentMode = 7;
        }
    }
}

void RotaryEncoder::decreaseMode() {
    if (currentMode == 0) {
        currentMode = 8;
    }
    currentMode--;

    if (!autoPilotAvailable) {
        if (currentMode > 2 && currentMode < 7) {
            currentMode = 2;
        }
    }
}

void RotaryEncoder::handleUpdate() {
    int8_t rotations = getRotations();

    if (rotations == 0) {
        return;
    }

    uint8_t identifier;
    switch (currentMode) {
        case ROTARY_ENCODER_MODE_TRIM_ELEVATOR:
            identifier = TRIM_ELEVATOR_ID;
            break;
        case ROTARY_ENCODER_MODE_TRIM_AILERONS:
            identifier = TRIM_AILERONS_ID;
            break;
        case ROTARY_ENCODER_MODE_TRIM_RUDDER:
            identifier = TRIM_RUDDER_ID;
            break;
        case ROTARY_ENCODER_MODE_AP_SPEED:
            identifier = AP_SPEED_ID;
            break;
        case ROTARY_ENCODER_MODE_AP_HEADING:
            identifier = AP_HEADING_ID;
            break;
        case ROTARY_ENCODER_MODE_AP_ALTITUDE:
            identifier = AP_ALTITUDE_ID;
            break;
        case ROTARY_ENCODER_MODE_AP_VSPEED:
            identifier = AP_VSPEED_ID;
            break;
        case ROTARY_ENCODER_MODE_E:
            identifier = CUSTOM_E_ID;
            break;
        default:
            return;
    }

    sendRelativeValue(identifier, rotations, 1);
    showRotaryStatus(getControlNameForIdentifier(identifier));
}
