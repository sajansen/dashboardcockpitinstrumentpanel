//
// Created by prive on 12/7/19.
//

#ifndef DASHBOARDCOCKPITINSTRUMENTPANEL_ROTARYENCODER_H
#define DASHBOARDCOCKPITINSTRUMENTPANEL_ROTARYENCODER_H

#include <Arduino.h>
#include "settings.h"
#include "utils.h"


#ifndef ROTARY_ENCODER_PIN_A      // Must be defined in settings.h
#define ROTARY_ENCODER_PIN_A 2      // Must be interrupt pin
#endif

#ifndef ROTARY_ENCODER_PIN_B      // Must be defined in settings.h
#define ROTARY_ENCODER_PIN_B 3      // Must be interrupt pin
#endif

#define ROTARY_ENCODER_MODE_TRIM_ELEVATOR 0
#define ROTARY_ENCODER_MODE_TRIM_AILERONS 1
#define ROTARY_ENCODER_MODE_TRIM_RUDDER 2
#define ROTARY_ENCODER_MODE_AP_SPEED 3
#define ROTARY_ENCODER_MODE_AP_HEADING 4
#define ROTARY_ENCODER_MODE_AP_ALTITUDE 5
#define ROTARY_ENCODER_MODE_AP_VSPEED 6
#define ROTARY_ENCODER_MODE_E 7


void rotaryEncoderInputChange();

class RotaryEncoder {

public:
    void setup();
    void clearRotations();
    int8_t getRotations();

    void increaseMode();
    void decreaseMode();

    void handleUpdate();

    uint8_t currentMode = 0;
    bool autoPilotAvailable = true;

    volatile int8_t rotaryEncoderTurns = 0;
};


extern RotaryEncoder rotaryEncoder;

#endif //DASHBOARDCOCKPITINSTRUMENTPANEL_ROTARYENCODER_H
