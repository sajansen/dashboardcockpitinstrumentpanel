//
// Created by prive on 9/6/19.
//

#include <Arduino.h>
#include <Wire.h>
#include <Adafruit_ADS1015.h>
#include <SPI.h>

#define ADC_SLIDERS_ADDRESS 0x48
#define ADC_FEET_PEDAL_ADDRESS 0x49
#define OUTPUT_BOARDS_LOAD_PIN 10


Adafruit_ADS1115 ADCsliders = Adafruit_ADS1115(ADC_SLIDERS_ADDRESS);



void setupSPI() {
    SPI.beginTransaction(SPISettings(
            1000000, /* 4 MHz */
            MSBFIRST,
            SPI_MODE0
    ));
    SPI.begin();
}


void digitalPulse(const uint8_t pin, const uint8_t pulseDirection) {
    digitalWrite(pin, pulseDirection);
    digitalWrite(pin, !pulseDirection);
}

void setOutputBoardValue(const uint8_t output_values) {
    SPI.transfer(output_values);
    digitalPulse(OUTPUT_BOARDS_LOAD_PIN, LOW);
}

void setOutputs(uint32_t status) {
    static uint32_t current_status = 0;

    if (status == current_status) {
        return;
    }
    current_status = status;

    setOutputBoardValue(status >> 16U);
    setOutputBoardValue(status >> 8U);
    setOutputBoardValue(status);
}

void clearAllOutputs() {
    setOutputs(0);
}

void setupOutputBoards() {
    pinMode(OUTPUT_BOARDS_LOAD_PIN, OUTPUT);
    digitalWrite(OUTPUT_BOARDS_LOAD_PIN, HIGH);
    clearAllOutputs();
}

void setupInputs() {
    ADCsliders.begin();
}

/**
 * @param pin
 * @param samples   Max value: 64
 * @return average reading value
 */
uint16_t analogReadAverage(const uint8_t pin, uint8_t samples) {
    uint16_t reading = 0;
    for (uint8_t i = 0; i < samples; i++) {
        reading += analogRead(pin);
    }
    return reading / samples;
}

uint16_t ADCRead(uint8_t address, uint8_t channel) {
    return ADCsliders.readADC_SingleEnded(channel);
}

void handleAnalogInputs() {
    for (uint8_t channel = 0; channel < 4; channel++) {
        setOutputs(0x00000001U << channel);
        // In...
        uint16_t value = ADCRead(ADC_SLIDERS_ADDRESS, channel);

        // Out...
        Serial.print(value);
        Serial.print("\t");
    }
    Serial.println("");
    setOutputs(0x00000001U << 8U);
}

void handleInputs() {
    handleAnalogInputs();
}


void singleRead() {
    Serial.print("Reading...  ");
    setOutputs(0x00000001U);
    uint16_t value = ADCsliders.readADC_SingleEnded(0);
    Serial.println(value);
    setOutputs(0x00000001U << 8U);
}

void setup() {
    Serial.begin(115200);
    setupSPI();
    setupOutputBoards();
    clearAllOutputs();
    setupInputs();
    Serial.println("Started.");

    setOutputs(0x00000001U);
    delay(200);
    setOutputs(0x00000001U << 8U);
    delay(200);
    clearAllOutputs();
    delay(100);
}

void loop() {
    handleInputs();
//    singleRead();
    delay(300);
}