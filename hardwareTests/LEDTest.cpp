#include <Arduino.h>
#include <SPI.h>

#define OUTPUT_BOARDS_LOAD_PIN 10


void digitalPulse(const uint8_t pin, const uint8_t pulseDirection) {
    digitalWrite(pin, pulseDirection);
    digitalWrite(pin, !pulseDirection);
}

void setBoardOutput(const uint8_t output_values) {
    SPI.transfer(output_values);
    digitalPulse(OUTPUT_BOARDS_LOAD_PIN, LOW);
}

void setupSPI() {
    SPI.beginTransaction(SPISettings(
            1000000, /* 4 MHz */
            MSBFIRST,
            SPI_MODE0
    ));
    SPI.begin();
}

void setupOutputBoards() {
    pinMode(OUTPUT_BOARDS_LOAD_PIN, OUTPUT);
    digitalWrite(OUTPUT_BOARDS_LOAD_PIN, HIGH);
    setBoardOutput(0);
}

void clearAll() {
    setBoardOutput(0);
    setBoardOutput(0);
    setBoardOutput(0);
}

void loopSingle() {
    uint32_t status = 1;

    for (uint8_t i = 0; i < 8 * 3; i++) {
        if (status == 0) {
            status = 1;
        }

        setBoardOutput(status >> 16);
        setBoardOutput(status >> 8);
        setBoardOutput(status);
        status <<= 1;
        delay(200);
    }
}

void flashAll() {
    setBoardOutput(0xff);
    setBoardOutput(0xff);
    setBoardOutput(0xff);
    delay(200);

    clearAll();
    delay(200);
}

void setup() {
    setupSPI();
    setupOutputBoards();
}

void loop() {
    loopSingle();

    clearAll();
    delay(200);

    flashAll();
}