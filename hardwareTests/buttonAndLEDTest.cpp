#include <Arduino.h>
#include <SPI.h>

#define INPUT_BOARDS_LOAD_PIN 9
#define OUTPUT_BOARDS_LOAD_PIN 10


uint8_t buttons[8] = {0};

void digitalPulse(const uint8_t pin, const uint8_t pulseDirection) {
    digitalWrite(pin, pulseDirection);
    digitalWrite(pin, !pulseDirection);
}

void enableButtonRows(const uint8_t output_values) {
    SPI.transfer(output_values);
    digitalPulse(INPUT_BOARDS_LOAD_PIN, LOW);
}

void setOutputBoardValue(const uint8_t output_values) {
    SPI.transfer(output_values);
    digitalPulse(OUTPUT_BOARDS_LOAD_PIN, LOW);
}

uint8_t readBoardInput() {
    digitalPulse(INPUT_BOARDS_LOAD_PIN, LOW);
    return SPI.transfer(0x00);
}

void setupSPI() {
    SPI.beginTransaction(SPISettings(
            1000000, /* 4 MHz */
            MSBFIRST,
            SPI_MODE0
    ));
    SPI.begin();
}

void setupButtonBoard() {
    pinMode(INPUT_BOARDS_LOAD_PIN, OUTPUT);
    digitalWrite(INPUT_BOARDS_LOAD_PIN, HIGH);
}

void collectButtons() {
    for (int i = 0; i < 8; i++) {
        enableButtonRows(1 << i);
        buttons[i] = readBoardInput();
    }
}

void clearAllOutputs() {
    setOutputs(0);
}

void setOutputs(uint32_t status) {
    static uint32_t current_status = 0;

    if (status == current_status) {
        return;
    }
    current_status = status;

    setOutputBoardValue(status >> 16U);
    setOutputBoardValue(status >> 8U);
    setOutputBoardValue(status);
}

void loopSingleOutputs() {
    uint32_t status = 1;

    for (uint8_t i = 0; i < 8 * 3; i++) {
        if (status == 0) {
            status = 1;
        }

        setOutputs(status);
        status <<= 1;
        delay(200);
    }
}

void flashAllOutputs() {
    clearAllOutputs();
    delay(100);

    setOutputs(0xffffffff);
    delay(80);

    clearAllOutputs();
    delay(100);
}

void setupOutputBoards() {
    pinMode(OUTPUT_BOARDS_LOAD_PIN, OUTPUT);
    digitalWrite(OUTPUT_BOARDS_LOAD_PIN, HIGH);
    clearAllOutputs();
}

void printByte(uint8_t b) {
    for (int i = 7; i >= 0; i--) {
        Serial.print(0x01 & (b >> i));
    }
    Serial.println("");
}

void setup() {
//    Serial.begin(115200);
    setupSPI();
    setupButtonBoard();
    setupOutputBoards();
//    Serial.println("Ready.");
}

void loop() {
    collectButtons();

    if (buttons[0] & 0x01) {
        flashAllOutputs();
    }

    uint32_t output = 0;
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            if (buttons[i] & (0x01 << j)) {
                uint8_t offset = (i * 8 + j) % 24;
                output |= (uint32_t) 0x01 << offset;
            }
        }
    }

    setOutputs(output);
}//
// Created by prive on 1/11/20.
//

