//
// Created by prive on 12/7/19.
//

#include <Arduino.h>

#define ROTARY_ENCODER_PIN_A 2      // Must be interrupt pin

#define ROTARY_ENCODER_PIN_B 3      // Must be interrupt pin

#define ROTARY_ENCODER_MODE_TRIM_ELEVATOR 0
#define ROTARY_ENCODER_MODE_TRIM_AILERONS 1
#define ROTARY_ENCODER_MODE_TRIM_RUDDER 2
#define ROTARY_ENCODER_MODE_AP_SPEED 3
#define ROTARY_ENCODER_MODE_AP_HEADING 4
#define ROTARY_ENCODER_MODE_AP_ALTITUDE 5
#define ROTARY_ENCODER_MODE_AP_VSPEED 6
#define ROTARY_ENCODER_MODE_E 7


void setup();
void clearRotations();
int8_t getRotations();

void increaseMode();
void decreaseMode();

void handleUpdate();

uint8_t currentMode = 0;

volatile int8_t rotaryEncoderTurns = 0;

void rotaryEncoderInputChange() {
    volatile static uint8_t rotationCycle = 0;
    volatile static bool initialPinAReading = true;
    volatile static bool initialPinBReading = true;

    bool pinAReading = digitalRead(ROTARY_ENCODER_PIN_A);
    bool pinBReading = digitalRead(ROTARY_ENCODER_PIN_B);

    // Reset everything for a clean new start
    if (pinAReading && pinBReading) {
        rotationCycle = 0;
        initialPinAReading = true;
        initialPinBReading = true;
        return;
    }
    // Don't go into the cycles if the last cycle is finished, but there isn't a clean new start yet (cycle=0)
    if (rotationCycle == 3) {
        return;
    }

    // Init new rotation, only when there is a fresh new start
    if (rotationCycle == 0 &&
        pinAReading != pinBReading &&
        initialPinAReading && initialPinBReading) {

        rotationCycle = 1;
        initialPinAReading = pinAReading;
        initialPinBReading = pinBReading;
        return;
    }
    // Don't go into the next cycles if the rotation hasn't been initialized (cycle=1)
    if (rotationCycle == 0) {
        return;
    }

    // Check if we pass the half way rotation mark
    if (!pinAReading && !pinBReading) {
        rotationCycle = 2;
        return;
    }
    // Don't go into the next cycles if we haven't passed the half way point yet (cycle=2)
    if (rotationCycle != 2) {
        return;
    }

    // If the knob is turned back, go to the first part of the rotation: the initialization (cycle=1)
    if (pinAReading == initialPinAReading && pinBReading == initialPinBReading) {
        rotationCycle = 1;
        return;
    }

    // Ok, we are through a full rotation
    rotationCycle = 3;

    rotaryEncoderTurns += pinAReading ? 1 : -1;
}


void setupRotary() {
    pinMode(ROTARY_ENCODER_PIN_A, INPUT_PULLUP);
    pinMode(ROTARY_ENCODER_PIN_B, INPUT_PULLUP);

    attachInterrupt(digitalPinToInterrupt(ROTARY_ENCODER_PIN_A), rotaryEncoderInputChange, CHANGE);
    attachInterrupt(digitalPinToInterrupt(ROTARY_ENCODER_PIN_B), rotaryEncoderInputChange, CHANGE);
}

void clearRotations() {
    rotaryEncoderTurns = 0;
}

int8_t getRotations() {
    if (rotaryEncoderTurns == 0) {
        return 0;
    }

    int8_t rotations = rotaryEncoderTurns;
    clearRotations();
    return rotations;
}

void increaseMode() {
    currentMode++;
    if (currentMode > 7) {
        currentMode = 0;
    }
}

void decreaseMode() {
    if (currentMode == 0) {
        currentMode = 8;
    }
    currentMode--;
}

void setup() {
    Serial.begin(115200);
    setupRotary();
    Serial.println("Started.");
}

void loop() {
    int8_t rotations = getRotations();

    if (rotations == 0) {
        return;
    }

    Serial.println(rotations);
    delay(100);
}