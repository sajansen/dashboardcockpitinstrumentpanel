//
// Created by prive on 9/6/19.
//

#include "identifiers.h"

const char * getControlNameForIdentifier(uint8_t identifier) {
    switch (identifier) {
        case NAVIGATION_LIGHTS_ID: return memStr(F("Navigation\nLights"));
        case BEACON_LIGHTS_ID: return memStr(F("Beacon\nLights"));
        case LANDING_LIGHTS_ID: return memStr(F("Landing\nLights"));
        case TAXI_LIGHTS_ID: return memStr(F("Taxi\nLights"));
        case STROBE_LIGHTS_ID: return memStr(F("Strobe\nLights"));
        case CABIN_LIGHTS_ID: return memStr(F("Cabin\nLights"));

        case MASTER_SWITCH_ID: return memStr(F("Master\nSwitch"));
        case LANDING_GEAR_ID: return memStr(F("Landing\nGear"));
        case PARKING_BRAKE_ID: return memStr(F("Parking\nBreak"));
        case KEYSTROKE_SIMULATION_PAUSE: return memStr(F("Pause"));

        case CUSTOM_A_ID: return memStr(F("Custom A"));
        case CUSTOM_B_ID: return memStr(F("Custom B"));
        case CUSTOM_C_ID: return memStr(F("Custom C"));
        case CUSTOM_D_ID: return memStr(F("Custom D"));
        case CUSTOM_E_ID: return memStr(F("Custom E"));
        case CUSTOM_G_ID: return memStr(F("Custom G"));

//        case FLAPS_INDEX_ID: return memStr(F("Flaps"));
//        case BRAKE_LEFT_ID: return memStr(F("Brake\nLeft"));
//        case BRAKE_RIGHT_ID: return memStr(F("Brake\nRight"));
//        case RUDDER_POSITION_ID: return memStr(F("Rudder"));
//        case SPOILER_CONTROL_ID: return memStr(F("Spoiler"));

        case TRIM_ELEVATOR_ID: return memStr(F("Elevator\nTrim"));
        case TRIM_AILERONS_ID: return memStr(F("Aileron\nTrim"));
        case TRIM_RUDDER_ID: return memStr(F("Rudder\nTrim"));
        case AP_SPEED_ID: return memStr(F("Auto Pilot\nSpeed"));
        case AP_HEADING_ID: return memStr(F("Auto Pilot\nHeading"));
        case AP_ALTITUDE_ID: return memStr(F("Auto Pilot\nAltitude"));
        case AP_VSPEED_ID: return memStr(F("Auto Pilot\nV. Speed"));

        case KEYSTROKE_WINDOW_ATC: return memStr(F("ATC"));
        case KEYSTROKE_WINDOW_1: return memStr(F("Window 1"));
        case KEYSTROKE_WINDOW_2: return memStr(F("Window 2"));
        case KEYSTROKE_WINDOW_3: return memStr(F("Window 3"));
        case KEYSTROKE_WINDOW_4: return memStr(F("Window 4"));
        case KEYSTROKE_WINDOW_5: return memStr(F("Window 5"));
        case KEYSTROKE_WINDOW_6: return memStr(F("Window 6"));
        case KEYSTROKE_WINDOW_7: return memStr(F("Window 7"));
        case KEYSTROKE_WINDOW_8: return memStr(F("Window 8"));
        case KEYSTROKE_WINDOW_9: return memStr(F("Window 9"));
        default:
            return memStr(F("Unnamed\nControl"));
    }
}
