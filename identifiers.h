//
// Created by Samuel on 31-8-2019.
//

#ifndef DASHBOARDCOCKPITINSTRUMENTPANEL_IDENTIFIERS_H
#define DASHBOARDCOCKPITINSTRUMENTPANEL_IDENTIFIERS_H

#include <Arduino.h>
#include "settings.h"
#include "utils.h"

// Define SimulatorData field with the corresponding identifier
/* Logic values */
#define NAVIGATION_LIGHTS_ID    1
#define BEACON_LIGHTS_ID        2
#define LANDING_LIGHTS_ID       3
#define TAXI_LIGHTS_ID          4
#define STROBE_LIGHTS_ID        5
#define PANEL_LIGHTS_ID         6
#define RECOGNITION_LIGHTS_ID   7
#define WING_LIGHTS_ID          8
#define LOGO_LIGHTS_ID          9
#define CABIN_LIGHTS_ID         10
#define TRIM_ELEVATOR_ID        11
#define TRIM_AILERONS_ID        12
#define TRIM_RUDDER_ID          13
#define AP_SPEED_ID             14
#define AP_HEADING_ID           15
#define AP_ALTITUDE_ID          16
#define AP_VSPEED_ID            17
#define CUSTOM_A_ID             18
#define CUSTOM_B_ID             19
#define CUSTOM_C_ID             20
#define CUSTOM_D_ID             21
#define CUSTOM_E_ID             22
#define CUSTOM_G_ID             23
#define MASTER_SWITCH_ID        24
#define PARKING_BRAKE_ID        25
#define LANDING_GEAR_ID         26


/* Analog values */
#define FLAPS_INDEX_ID          100     // 0x64
#define BRAKE_LEFT_ID           101
#define BRAKE_RIGHT_ID          102
#define RUDDER_POSITION_ID      103
#define SPOILER_CONTROL_ID      104
#define CUSTOM_F_ID             105
#define FEET_PEDAL_LEFT_ID      106
#define FEET_PEDAL_RIGHT_ID     107

/* Keystrokes */
#define KEYSTROKE_WINDOW_ATC    150
#define KEYSTROKE_WINDOW_1      151
#define KEYSTROKE_WINDOW_2      152
#define KEYSTROKE_WINDOW_3      153
#define KEYSTROKE_WINDOW_4      154
#define KEYSTROKE_WINDOW_5      155
#define KEYSTROKE_WINDOW_6      156
#define KEYSTROKE_WINDOW_7      157
#define KEYSTROKE_WINDOW_8      158
#define KEYSTROKE_WINDOW_9      159
#define KEYSTROKE_SIMULATION_PAUSE  160

/* Internal commands */
#define ROTARY_ENCODER_MODE_UP      200
#define ROTARY_ENCODER_MODE_DOWN    201

const uint8_t inputMatrixSwitches[] = {
        NAVIGATION_LIGHTS_ID,
        BEACON_LIGHTS_ID,
        TAXI_LIGHTS_ID,
        LANDING_LIGHTS_ID,
        STROBE_LIGHTS_ID,
        CABIN_LIGHTS_ID,
        MASTER_SWITCH_ID,
        CUSTOM_G_ID,
        PARKING_BRAKE_ID,
        LANDING_GEAR_ID,
};

const uint8_t inputIdentifier[8][8] = {
        {KEYSTROKE_WINDOW_2, KEYSTROKE_WINDOW_3, KEYSTROKE_WINDOW_4, KEYSTROKE_WINDOW_5, KEYSTROKE_WINDOW_6, KEYSTROKE_WINDOW_7, KEYSTROKE_WINDOW_8, KEYSTROKE_WINDOW_9},
        {KEYSTROKE_WINDOW_ATC, KEYSTROKE_WINDOW_1, BEACON_LIGHTS_ID, LANDING_LIGHTS_ID, TAXI_LIGHTS_ID, NAVIGATION_LIGHTS_ID, STROBE_LIGHTS_ID, CABIN_LIGHTS_ID},
        {CUSTOM_A_ID, CUSTOM_B_ID, CUSTOM_C_ID, CUSTOM_D_ID, CUSTOM_G_ID, PARKING_BRAKE_ID, MASTER_SWITCH_ID, LANDING_GEAR_ID},
        {KEYSTROKE_SIMULATION_PAUSE, ROTARY_ENCODER_MODE_UP, ROTARY_ENCODER_MODE_DOWN, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0},
};

const uint8_t ADCAdresses[ADC_AMOUNT] = {
        ADC_SLIDERS_ADDRESS,
        ADC_FEET_PEDAL_ADDRESS,
};
const uint8_t ADCIdentifiers[ADC_AMOUNT][4] = {
        {
                FLAPS_INDEX_ID,
                SPOILER_CONTROL_ID,
                CUSTOM_F_ID,
                0
        },
        {
                FEET_PEDAL_LEFT_ID,
                FEET_PEDAL_RIGHT_ID,
                0,
                0
        }
};

const char *getControlNameForIdentifier(uint8_t identifier);

#endif //DASHBOARDCOCKPITINSTRUMENTPANEL_IDENTIFIERS_H
