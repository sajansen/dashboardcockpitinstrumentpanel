//
// Created by prive on 9/6/19.
//

#include "inputs.h"
#include "ADS1115.h"


InputMatrix inputMatrix;
ADS1115 ADCsliders = ADS1115(ADC_SLIDERS_ADDRESS);
ADS1115 ADCfeetPedal = ADS1115(ADC_FEET_PEDAL_ADDRESS);

uint16_t flapsCalibration[MAX_FLAP_POSITION_NUMBER + 1] = {
        24290,  // Lower value of the first position (0)
        16142,  // Lower value of the second poisiton (1)
        9000,  // etc.. 2
        5738,  // 3
        3820,  // 4
        1790,  // 5
        730,  // 6
        329,  // 7
        60,  // 8
        0  // Lower value of the last position (9)
};

void setupInputs() {
    inputMatrix.setup();
    rotaryEncoder.setup();

    ADCsliders.setup();
    ADCfeetPedal.setup();
}

void handleInputs() {
    static unsigned long nextUpdateTimeRotaryEncoder = 0;
    handleInputMatrixInputs();

    handleAnalogInputs();

    if (millis() > nextUpdateTimeRotaryEncoder) {
        nextUpdateTimeRotaryEncoder = millis() + ROTARY_ENCODER_UPDATE_INTERVAL;
        rotaryEncoder.handleUpdate();
    }
}

void handleInputMatrixInputs() {
    inputMatrix.readAll();

    if (!inputMatrix.hasStateChanged()) return;

    for (uint8_t row = 0; row < 8; row++) {
        if (!inputMatrix.hasRowChanged(row)) continue;

        for (uint8_t column = 0; column < 8; column++) {
            if (!inputMatrix.hasValueChanged(row, column)) continue;

            inputMatrix.sendValueUpdate(row, column);
        }
    }
}

uint8_t getFlapIndexForSliderValue(uint16_t value) {
    uint8_t flapsIndex = 0;
    for (; flapsIndex < MAX_FLAP_POSITION_NUMBER; flapsIndex++) {
        if (value > flapsCalibration[flapsIndex]){
            return flapsIndex;
        }
    }
    return flapsIndex;
}

void filterAnalogInput(uint8_t identifier, uint16_t * value) {
    if (identifier == FLAPS_INDEX_ID) {
        *value = getFlapIndexForSliderValue(*value);
        return;
    }

    if (identifier == SPOILER_CONTROL_ID) {
        *value = getFlapIndexForSliderValue(*value);
        return;
    }

    *value >>= ADC_SAMPLE_REDUCTION_BIT_SHIFT;
}

void handleAnalogInputs() {
    static uint16_t prevReadings[ADC_AMOUNT][4] = {0};

    for (uint8_t i = 0; i < ADC_AMOUNT; i++) {
        for (uint8_t channel = 0; channel < 4; channel++) {
            uint8_t identifier = ADCIdentifiers[i][channel];
            if (identifier == 0) {
                continue;
            }

            // In...
            uint32_t totalValue = 0;
            for (uint8_t sample = 0; sample < ADC_SAMPLE_AVERAGE_COUNT; sample++) {
                totalValue += ADCRead(ADCAdresses[i], channel);
            }
            uint16_t value = totalValue / ADC_SAMPLE_AVERAGE_COUNT;
            filterAnalogInput(identifier, &value);

            if (value == prevReadings[i][channel]) {
                if (OPERATION_MODE != OPERATION_MODE_CALIBRATION) {
                    continue;
                }
            }
            prevReadings[i][channel] = value;

            // Out...
            sendAbsoluteValue(identifier, value, 2);
        }
    }
}

uint16_t ADCRead(uint8_t address, uint8_t channel) {
    if (address == ADC_SLIDERS_ADDRESS) {
        return ADCsliders.readChannel(channel);
    } else if (address == ADC_FEET_PEDAL_ADDRESS) {
#if ADC_DISABLE_FEET_PEDALS
        return 0;
#endif
        return ADCfeetPedal.readChannel(channel);
    }
}
