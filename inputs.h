//
// Created by prive on 9/6/19.
//

#ifndef DASHBOARDCOCKPITINSTRUMENTPANEL_INPUTS_H
#define DASHBOARDCOCKPITINSTRUMENTPANEL_INPUTS_H

#include <Arduino.h>
#include "InputMatrix.h"
#include "settings.h"
#include "identifiers.h"
#include "utils.h"
#include "RotaryEncoder.h"

#ifndef ADC_SAMPLE_REDUCTION_BIT_SHIFT
#define ADC_SAMPLE_REDUCTION_BIT_SHIFT 5U
#endif

#ifndef MAX_FLAP_POSITION_NUMBER
#define MAX_FLAP_POSITION_NUMBER 9
#endif

#ifndef ROTARY_ENCODER_UPDATE_INTERVAL
#define ROTARY_ENCODER_UPDATE_INTERVAL 300
#endif

void setupInputs();
void handleInputs();
void handleInputMatrixInputs();
void handleAnalogInputs();
uint16_t ADCRead(uint8_t address, uint8_t channel);

#endif //DASHBOARDCOCKPITINSTRUMENTPANEL_INPUTS_H
