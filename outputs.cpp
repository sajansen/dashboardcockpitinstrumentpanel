//
// Created by Samuel on 26-8-2019.
//

#include "outputs.h"

/*
 * Output boards mapping:
 *   [0] [rotary:7-0]
 *   [1] [flaps:7-0]
 *   [2] [buttonB:7, buttonA:6, landingGear:5-2, flaps:1-0]
 */
uint8_t outputBoardStates[OUTPUT_BOARDS_AMOUNT] = {0};

/*
 * Landing gear statuses.
 */
uint8_t landingGearLeftStatus = LANDING_GEAR_STATUS_DOWN;
uint8_t landingGearFrontStatus = LANDING_GEAR_STATUS_DOWN;
uint8_t landingGearRightStatus = LANDING_GEAR_STATUS_DOWN;


void setupOutputs() {
    pinMode(BACKLIGHT_PIN, OUTPUT);

    setupOutputBoards();
    setupFeedBackMotor();
}

void handleLanding(SimulatorData *simulatorData) {
    static bool isPlaneOnGround = true;

    if (isPlaneOnGround == simulatorData->PLANE_ON_GROUND) return;
    isPlaneOnGround = simulatorData->PLANE_ON_GROUND;

    if (!isPlaneOnGround) return;

    // Plane just landed
    startFeedbackMotor(FEEDBACK_MOTOR_LANDING_DURATION);
}


void handleCrash(SimulatorData *simulatorData) {
    static bool isCrashed = false;
    if (simulatorData->CRASHED != isCrashed) {
        isCrashed = simulatorData->CRASHED;

        if (isCrashed) {
            startFeedbackMotor(FEEDBACK_MOTOR_CRASHED_DURATION);
        }
    }
}

void handleOverspeed(SimulatorData *simulatorData) {
    if (!simulatorData->OVERSPEED || simulatorData->INDICATED_AIR_SPEED <= simulatorData->MAX_AIRSPEED) {
        return;
    }

    const uint32_t excessAirspeed = simulatorData->INDICATED_AIR_SPEED - simulatorData->MAX_AIRSPEED;
    const uint8_t overspeed_percentage = min(255, 255 * excessAirspeed / (simulatorData->MAX_AIRSPEED / 5));
    startFeedbackMotor(SIMULATOR_DATA_UPDATE_INTERVAL, overspeed_percentage);
}

void handleTurbulence(SimulatorData *simulatorData) {
    static unsigned long nextTurbulentTime = 0;
    if (!simulatorData->CLOUD_TURBULENCE) return;

    if (millis() < nextTurbulentTime) return;

    startFeedbackMotor((pow(2, random(0, 1500) / 1000.0) - 0.8) * 1000, random(50, 255));

    nextTurbulentTime = millis() + (pow(2, random(0, 5000) / 1000.0) - 1) * 1000;
}

void handleLandingGearUpdate(SimulatorData *simulatorData) {
    static uint16_t previousGearActualLeft = 0;
    static uint16_t previousGearActualFront = 0;
    static uint16_t previousGearActualRight = 0;

    landingGearLeftStatus = getLandingGearStatusForValues(previousGearActualLeft, simulatorData->GEAR_ACTUAL_LEFT);
    landingGearFrontStatus = getLandingGearStatusForValues(previousGearActualFront, simulatorData->GEAR_ACTUAL_FRONT);
    landingGearRightStatus = getLandingGearStatusForValues(previousGearActualRight, simulatorData->GEAR_ACTUAL_RIGHT);

    previousGearActualLeft = simulatorData->GEAR_ACTUAL_LEFT;
    previousGearActualFront = simulatorData->GEAR_ACTUAL_FRONT;
    previousGearActualRight = simulatorData->GEAR_ACTUAL_RIGHT;

    bool isGearMoving = landingGearLeftStatus == LANDING_GEAR_STATUS_MOVING
                        || landingGearFrontStatus == LANDING_GEAR_STATUS_MOVING
                        || landingGearRightStatus == LANDING_GEAR_STATUS_MOVING;

    if (!isGearMoving) {
        return;
    }

    // Handle feedback motor with dynamic force
    uint8_t feedbackMotorForce = 100;
    feedbackMotorForce += landingGearLeftStatus == LANDING_GEAR_STATUS_MOVING ? 25 : 0;
    feedbackMotorForce += landingGearFrontStatus == LANDING_GEAR_STATUS_MOVING ? 25 : 0;
    feedbackMotorForce += landingGearRightStatus == LANDING_GEAR_STATUS_MOVING ? 25 : 0;
    startFeedbackMotor(SIMULATOR_DATA_UPDATE_INTERVAL, feedbackMotorForce);
}

uint8_t getLandingGearStatusForValues(uint16_t previousValue, uint16_t newValue) {
    if (newValue != previousValue) {
        return LANDING_GEAR_STATUS_MOVING;
    }
    if (newValue == 0) {
        return LANDING_GEAR_STATUS_UP;
    }
    if (newValue > 16381) {
        return LANDING_GEAR_STATUS_DOWN;
    }
    return LANDING_GEAR_STATUS_STUCK;
}

void handleLandingGearCycle() {
    static unsigned long nextBlinkTransition = 0;
    static bool isLEDCycleStateOn = false;
    bool isLEDStateLeftOn = false;
    bool isLEDStateFrontOn = false;
    bool isLEDStateRightOn = false;
    bool isLEDStateErrorOn = false;

    // Handle LED blink
    if (millis() < nextBlinkTransition) {
        return;
    }
    nextBlinkTransition = millis() + LANDING_GEAR_ERROR_BLINK_INTERVAL;
    isLEDCycleStateOn = !isLEDCycleStateOn;

    if (landingGearLeftStatus == LANDING_GEAR_STATUS_DOWN) {
        isLEDStateLeftOn = true;
    } else if (landingGearLeftStatus == LANDING_GEAR_STATUS_STUCK) {
        isLEDStateLeftOn = isLEDCycleStateOn;
        isLEDStateErrorOn = isLEDCycleStateOn;
    }
    if (landingGearFrontStatus == LANDING_GEAR_STATUS_DOWN) {
        isLEDStateFrontOn = true;
    } else if (landingGearFrontStatus == LANDING_GEAR_STATUS_STUCK) {
        isLEDStateFrontOn = isLEDCycleStateOn;
        isLEDStateErrorOn = isLEDCycleStateOn;
    }
    if (landingGearRightStatus == LANDING_GEAR_STATUS_DOWN) {
        isLEDStateRightOn = true;
    } else if (landingGearRightStatus == LANDING_GEAR_STATUS_STUCK) {
        isLEDStateRightOn = isLEDCycleStateOn;
        isLEDStateErrorOn = isLEDCycleStateOn;
    }

    if (landingGearLeftStatus == LANDING_GEAR_STATUS_MOVING
        || landingGearFrontStatus == LANDING_GEAR_STATUS_MOVING
        || landingGearRightStatus == LANDING_GEAR_STATUS_MOVING) {
        isLEDStateErrorOn = true;
    }

    setLandingGearStatusLEDs(isLEDStateLeftOn, isLEDStateFrontOn, isLEDStateRightOn, isLEDStateErrorOn);
}

void setLandingGearStatusLEDs(bool left, bool front, bool right, bool error) {
    // Clear landing status with a mask
    outputBoardStates[2] &= 0b11000011U;
    outputBoardStates[2] |= error << LED_BIT_POSITION_LANDING_GEAR_ERROR;
    outputBoardStates[2] |= left << LED_BIT_POSITION_LANDING_GEAR_LEFT;
    outputBoardStates[2] |= front << LED_BIT_POSITION_LANDING_GEAR_FRONT;
    outputBoardStates[2] |= right << LED_BIT_POSITION_LANDING_GEAR_RIGHT;
}

void handleFlapsPosition(SimulatorData *simulatorData) {
    if (simulatorData->FLAPS_POSITIONS == 0) {
        setFlapsStatusLEDs(0);
        return;
    }

    uint16_t LEDpositions = 0;

    float LEDinterval = MAX_FLAP_POSITION_NUMBER / ((float) simulatorData->FLAPS_POSITIONS);
    if (LEDinterval == 0) {
        LEDinterval = 1;
    }

    float i = 0;
    while (i <= 9.0) {
        LEDpositions |= 1U << round(i);
        i += LEDinterval;
    }

    setFlapsStatusLEDs(LEDpositions);
}

void setFlapsStatusLEDs(uint16_t state) {
    outputBoardStates[1] = (uint8_t) state;
    // Clear flaps status with a mask
    outputBoardStates[2] &= 0b11111100U;
    outputBoardStates[2] |= state >> 8U;
}

void handleRotaryEncoderMode() {
    outputBoardStates[0] = 1U << rotaryEncoder.currentMode;
}

void handleBacklight(SimulatorData *simulatorData) {
    if (OPERATION_MODE == OPERATION_MODE_UNCONNECTED) {
        if (!inputMatrix.getNewValueForIdentifier(MASTER_SWITCH_ID)) {
            analogWrite(BACKLIGHT_PIN, 0);
        } else {
            analogWrite(BACKLIGHT_PIN, 100);
        }
    } else {
        analogWrite(BACKLIGHT_PIN, simulatorData->BACKLIGHT);
    }
}

void handleCustomButtons(SimulatorData *simulatorData) {
    // Clear status
    outputBoardStates[2] &= ~0b11000000U;

    if (simulatorData->LED_CUSTOM_A) {
        outputBoardStates[2] |= 1U << LED_BIT_POSITION_CUSTOM_A;
    }
    if (simulatorData->LED_CUSTOM_B) {
        outputBoardStates[2] |= 1U << LED_BIT_POSITION_CUSTOM_B;
    }
}

void setupOutputBoards() {
    pinMode(OUTPUT_BOARDS_LOAD_PIN, OUTPUT);
    digitalWrite(OUTPUT_BOARDS_LOAD_PIN, HIGH);

    updateOutputBoards();
}

void updateOutputBoards() {
    static uint8_t previousOutputBoardStates[OUTPUT_BOARDS_AMOUNT] = {0};

    bool needsUpdate = false;
    for (int8_t i = 0; i < OUTPUT_BOARDS_AMOUNT; i++) {
        needsUpdate |= outputBoardStates[i] != previousOutputBoardStates[i];
    }

    if (OPERATION_MODE == OPERATION_MODE_UNCONNECTED && !inputMatrix.getNewValueForIdentifier(MASTER_SWITCH_ID)) {
        for (unsigned char & outputBoardState : outputBoardStates) {
            outputBoardState = 0;
        }
    } else if (!needsUpdate) {
        return;
    }

    for (int8_t i = OUTPUT_BOARDS_AMOUNT; i > 0; i--) {
        SPI.transfer(outputBoardStates[i - 1]);
    }

    digitalPulse(OUTPUT_BOARDS_LOAD_PIN, LOW);
}

void handleOutputBoardsCycle() {
    handleRotaryEncoderMode();
    updateOutputBoards();
}