//
// Created by Samuel on 26-8-2019.
//

#ifndef SERIALTOKEYBOARDSTROKESDEVICE_OUTPUTS_H
#define SERIALTOKEYBOARDSTROKESDEVICE_OUTPUTS_H

#include <Arduino.h>
#include <SPI.h>
#include "settings.h"
#include "FeedbackMotor.h"
#include "utils.h"
#include "RotaryEncoder.h"

#ifndef MAX_FLAP_POSITION_NUMBER
#define MAX_FLAP_POSITION_NUMBER 9
#endif

#ifndef LANDING_GEAR_ERROR_BLINK_INTERVAL
#define LANDING_GEAR_ERROR_BLINK_INTERVAL 600
#endif

#define LED_BIT_POSITION_LANDING_GEAR_ERROR 2U
#define LED_BIT_POSITION_LANDING_GEAR_LEFT 3U
#define LED_BIT_POSITION_LANDING_GEAR_FRONT 4U
#define LED_BIT_POSITION_LANDING_GEAR_RIGHT 5U
#define LED_BIT_POSITION_CUSTOM_A 6U
#define LED_BIT_POSITION_CUSTOM_B 7U

#define LANDING_GEAR_STATUS_DOWN    0
#define LANDING_GEAR_STATUS_MOVING  1
#define LANDING_GEAR_STATUS_UP      2
#define LANDING_GEAR_STATUS_STUCK   3

void setupOutputs();

void handleLanding(SimulatorData *simulatorData);
void handleCrash(SimulatorData* simulatorData);
void handleOverspeed(SimulatorData* simulatorData);
void handleLandingGearUpdate(SimulatorData *simulatorData);
void handleTurbulence(SimulatorData *simulatorData);
void handleFlapsPosition(SimulatorData *simulatorData);
void handleCustomButtons(SimulatorData *simulatorData);
void handleRotaryEncoderMode();
void handleBacklight(SimulatorData *simulatorData);

void setFlapsStatusLEDs(uint16_t state);
uint8_t getLandingGearStatusForValues(uint16_t previousValue, uint16_t newValue);
void handleLandingGearCycle();
void setLandingGearStatusLEDs(bool left, bool front, bool right, bool error);
void setupOutputBoards();
void updateOutputBoards();
void handleOutputBoardsCycle();

#endif //SERIALTOKEYBOARDSTROKESDEVICE_OUTPUTS_H
