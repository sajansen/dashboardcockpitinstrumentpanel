//
// Created by prive on 9/5/19.
//

#include "screen.h"

Adafruit_SH1106 display(-1);

const uint8_t bottomHalfY = display.height() - TEXT_SIZE_HEIGHT * SCREEN_BOTTOM_HALF_TEXT_SIZE -
                            (SCREEN_BOTTOM_HALF_PADDING_TOP +
                             SCREEN_BOTTOM_HALF_PADDING_BOTTOM);  // display height - text height (size 3) - 2 * paddings

unsigned long nextClearTime = 0;


void setupScreen() {
    display.begin(SH1106_SWITCHCAPVCC, 0x3C);

    display.clearDisplay();
    display.setTextColor(WHITE);
    display.setCursor(0, 0);
    display.print(F("Booting..."));
    display.display();
}

void drawTopHalfControlName(const char *name, const bool isBacklightOn) {
    if (isBacklightOn) {
        display.fillRect(0, 0, display.width(), bottomHalfY - 1, WHITE);
        display.setTextColor(BLACK);
    } else {
        display.setTextColor(WHITE);
    }
    display.setTextSize(2);
    display.setTextWrap(true);
    display.setCursor(0, 0);
    display.print(name);
}

void drawTopHalfControlName(const char *name) { drawTopHalfControlName(name, false); }

void drawBottomHalfWithValue(const char *text, const float textLeftIndent, const bool isBacklightOn) {
    display.drawFastHLine(0, bottomHalfY, display.width(), WHITE);
    if (isBacklightOn) {
        display.fillRect(0, bottomHalfY, display.width(), display.height() - bottomHalfY, WHITE);
        display.setTextColor(BLACK);
    } else {
        display.setTextColor(WHITE);
    }

    display.setCursor(textLeftIndent * TEXT_SIZE_WIDTH * SCREEN_BOTTOM_HALF_TEXT_SIZE,
                      bottomHalfY + SCREEN_BOTTOM_HALF_PADDING_TOP);
    display.setTextSize(3);
    display.print(text);
}

void showButtonStatus(const char *name) {
    display.clearDisplay();

    display.setTextColor(WHITE);
    display.setTextSize(1);
    display.setTextWrap(true);
    display.setCursor((display.width() - 7 * TEXT_SIZE_WIDTH) / 2, 0);
    display.print("BUTTON");

    display.setTextColor(WHITE);
    display.setCursor(0, TEXT_SIZE_HEIGHT + 6);
    display.setTextSize(2);
    display.setTextWrap(true);
    display.print(name);

    display.display();
    screenResetDisplayTimer();
}

void showSwitchStatus(const char *name, const bool isOn, const bool isHighlighted) {
    display.clearDisplay();
    drawTopHalfControlName(name, isHighlighted);
    drawBottomHalfWithValue(isOn ? memStr(F("On")) : memStr(F("Off")), isOn ? 3 : 2.5, isOn);

    display.display();
    screenResetDisplayTimer();
}

void showSwitchStatus(const char *name, const bool isOn) { showSwitchStatus(name, isOn, false); }

void showSliderStatus(const char *name, const int8_t percentage, const bool isHighlighted) {
    display.clearDisplay();
    drawTopHalfControlName(name, isHighlighted);
    char percentage_text[5];
    sprintf(percentage_text, "%3d%%", percentage);
    drawBottomHalfWithValue(percentage_text, 2, false);

    display.display();
    screenResetDisplayTimer();
}

void showSliderStatus(const char *name, const int8_t percentage) { showSliderStatus(name, percentage, false); }

void showHighlightedSliderStatus(const char *name, const int8_t percentage) {
    showSliderStatus(name, percentage, true);
}

void showFractionStatus(const char *name, const int8_t numerator, const int8_t denominator, const bool isHighlighted) {
    display.clearDisplay();
    drawTopHalfControlName(name, isHighlighted);
    char percentage_text[8];
    sprintf(percentage_text, "%3d/%d", numerator, denominator);
    drawBottomHalfWithValue(percentage_text, 0, false);

    display.display();
    screenResetDisplayTimer();
}

void showFractionStatus(const char *name, const int8_t numerator, const int8_t denominator) {
    showFractionStatus(name, numerator, denominator, false);
}

void showHighlightedFractionStatus(const char *name, const int8_t numerator, const int8_t denominator) {
    showFractionStatus(name, numerator, denominator, true);
}

void showRotaryStatus(const char *name) {
    display.clearDisplay();

    display.setTextColor(WHITE);
    display.setTextSize(1);
    display.setTextWrap(true);
    display.setCursor((display.width() - 7 * TEXT_SIZE_WIDTH) / 2, 0);
    display.print("ROTARY");

    display.setTextColor(WHITE);
    display.setCursor(0, TEXT_SIZE_HEIGHT + 6);
    display.setTextSize(2);
    display.setTextWrap(true);
    display.print(name);

    display.display();
    screenResetDisplayTimer();
}

void showSpeedStatus(const char *name, const int16_t speed) {
#if DEMO_MODE
    display.clearDisplay();

    drawTopHalfControlName(name);

    char speed_text[6];
    sprintf(speed_text, "%5d", speed);
    drawBottomHalfWithValue(speed_text, 0, false);
    display.setTextSize(1);
    display.print(F(" knts"));

    display.display();
    screenResetDisplayTimer();
#endif
}

void showAltitudeStatus(const char *name, const int16_t height) {
#if DEMO_MODE
    display.clearDisplay();

    drawTopHalfControlName(name);

    char height_text[7];
    sprintf(height_text, "%6d", height);
    drawBottomHalfWithValue(height_text, 0, false);
    display.setTextSize(1);
    display.print(" ft");

    display.display();
    screenResetDisplayTimer();
#endif
}

void showDashboard() {
    display.clearDisplay();

    display.drawBitmap(0, 12, airplaneBitmap, 128, 44, WHITE);
    display.display();
}

void showWarningMessage(const char *text) {
    display.clearDisplay();

    display.fillRect(0, 0, display.width(), 1, WHITE);
    display.fillRect(display.width() - 3, 0, 3, display.height(), WHITE);
    display.fillRect(0, display.height() - 2, display.width(), display.height(), WHITE);

    display.setTextColor(WHITE);
    display.setTextSize(2);
    display.setTextWrap(true);
    display.setCursor(1, 5);
    display.print(text);

    display.display();
    screenResetDisplayTimer();
}

void showErrorMessage(const char *text) {
    display.clearDisplay();

    display.fillScreen(WHITE);

    display.setTextColor(BLACK);
    display.setTextSize(2);
    display.setTextWrap(true);
    display.setCursor(2, 10);
    display.print(text);

    display.display();
    screenResetDisplayTimer();
}

void showBlackScreen(const char *text) {
    display.clearDisplay();

    display.setTextColor(WHITE);
    display.setTextSize(1);
    display.setTextWrap(true);
    display.setCursor(0, 0);
    display.print(text);

    display.display();
    screenResetDisplayTimer();
}

void screenResetDisplayTimer() {
    nextClearTime = millis() + SCREEN_DISPLAY_TIME;
}

void screenHandleDisplayTimer() {
    if (nextClearTime == 0) return;     // Don't do anything further, because it's unnecessary
    if (millis() < nextClearTime) return;
    nextClearTime = 0;

    showDashboard();
}

void handleScreenCycle() {
    screenHandleDisplayTimer();
}

void showDataUpdateInProgress(bool isInProgress) {
    display.drawPixel(display.width() - 1, display.height() - 1, (isInProgress ? WHITE : BLACK));
    display.display();
}