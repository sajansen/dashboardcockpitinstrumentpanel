//
// Created by Samuel on 26-8-2019.
//

#ifndef SERIALTOKEYBOARDSTROKESDEVICE_SETTINGS_H
#define SERIALTOKEYBOARDSTROKESDEVICE_SETTINGS_H

#include <Arduino.h>

#define DEMO_MODE false
#define DEMO_SERIAL_CONNECTION false
#define ADC_DISABLE_FEET_PEDALS false        // Set to true if the ADC for the feet pedal isn't connected, to prevent waiting for timeouts

// Serial connection
#define BAUD_RATE 115200
#define SIMULATOR_DATA_UPDATE_INTERVAL    400   // in milliseconds
#define SERIAL_META_START_BITS 0x05U    // The first constant 3-bits to identify the start of the message
#define SERIAL_DATA_TYPE_TOGGLE 0
#define SERIAL_DATA_TYPE_BOOLEAN 1
#define SERIAL_DATA_TYPE_ABSOLUTE_VALUE 2
#define SERIAL_DATA_TYPE_RELATIVE_VALUE 3
#define SERIAL_DATA_TYPE_UPDATE_REQUEST 4

#define OPERATION_MODE_UNCONNECTED 0
#define OPERATION_MODE_SIMULATOR 1
#define OPERATION_MODE_CALIBRATION 2

// Pins
#define ROTARY_ENCODER_PIN_A 2      // Must be interrupt pin
#define ROTARY_ENCODER_PIN_B 3      // Must be interrupt pin
#define INPUT_MATRIX_LOAD_PIN 9
#define OUTPUT_BOARDS_LOAD_PIN 10
#define FEEDBACK_MOTOR_PIN 6
#define BACKLIGHT_PIN 5

/* Inputs */
#define ADC_AMOUNT 2
#define ADC_SLIDERS_ADDRESS 0x48
#define ADC_FEET_PEDAL_ADDRESS 0x49
#define ADC_SAMPLE_AVERAGE_COUNT 5
#define ADC_SAMPLE_REDUCTION_BIT_SHIFT 4U
#define MAX_FLAP_POSITION_NUMBER 9
#define ROTARY_ENCODER_UPDATE_INTERVAL 100

/* Outputs */
#define OUTPUT_BOARDS_AMOUNT 3
#define FEEDBACK_MOTOR_LANDING_DURATION  1000    // in milliseconds
#define LANDING_GEAR_ERROR_BLINK_INTERVAL 100     // in milliseconds
#define FEEDBACK_MOTOR_CRASHED_DURATION  4000    // in milliseconds
#define SCREEN_DISPLAY_TIME 2500                 // in milliseconds


// The to be requested data (in order)
struct SimulatorData {
    uint8_t OPERATION_MODE;
    uint8_t LED_CUSTOM_A;
    uint8_t LED_CUSTOM_B;
    uint8_t BACKLIGHT;
    uint32_t MAX_AIRSPEED;
    uint8_t AUTO_PILOT_AVAILABLE;
    int32_t INDICATED_AIR_SPEED;
    int16_t GEAR_ACTUAL_LEFT;
    int16_t GEAR_ACTUAL_FRONT;
    int16_t GEAR_ACTUAL_RIGHT;
//    int32_t SPOILER_CONTROL;
//  int32_t   GROUND_SPEED;
//    uint16_t FLAPS_DETENT_INCREMENT;
    uint8_t FLAPS_POSITIONS;
    uint8_t PLANE_ON_GROUND;
    uint16_t CLOUD_TURBULENCE;
//  uint16_t  BRAKE_LEFT;
//  uint16_t  BRAKE_RIGHT;
    int8_t CRASHED;
    int8_t PAUSE_FLAG;
    uint8_t OVERSPEED;
//    uint8_t FLAPS_INDEX;
//    uint8_t NAVIGATION_LIGHTS;
//    uint8_t BEACON_LIGHTS;
//    uint8_t LANDING_LIGHTS;
//    uint8_t TAXI_LIGHTS;
//    uint8_t STROBE_LIGHTS;
//    uint8_t PANEL_LIGHTS;
//    uint8_t RECOGNITION_LIGHTS;
//    uint8_t CABIN_LIGHTS;
//    uint8_t LOGO_LIGHTS;
//    uint8_t WING_LIGHTS;
};

extern struct SimulatorData simulatorData;
extern uint8_t OPERATION_MODE;


#endif //SERIALTOKEYBOARDSTROKESDEVICE_SETTINGS_H
