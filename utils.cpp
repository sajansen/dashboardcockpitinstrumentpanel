//
// Created by Samuel on 26-8-2019.
//

#include "utils.h"

/**
 * Empty the Serial input buffer
 */
void serialFlush() {
    while (Serial.available() > 0) {
        Serial.read();
    }
}

/**
 * Quickly pulse a pin
 * @param pin
 * @param pulseDirection
 */
void digitalPulse(const uint8_t pin, const uint8_t pulseDirection) {
    digitalWrite(pin, pulseDirection);
    digitalWrite(pin, !pulseDirection);
}

/**
 * Get a string from the PROGMEM by passing the PROGMEM assignment value as argument, e.g. memStr( F("my string") )
 * @param ifsh
 * @return
 */
const char *memStr(const __FlashStringHelper *ifsh) {
    const char * __attribute__((progmem)) p = (const char * ) ifsh;
    static char *buffer;
    free(buffer);

    int stringSize = 0;
    for (;;) {
        unsigned char c = pgm_read_byte(p + stringSize);
        if (c == 0) break;
        stringSize++;
    }

    if (stringSize == 0)
        return nullptr;

    buffer = (char*) malloc((stringSize + 1) * sizeof(char));

    for (int i = 0; i < stringSize; i++) {
        buffer[i] = pgm_read_byte(p + i);
    }

    buffer[stringSize] = '\0';
    return buffer;
}

/*
 * Sending the data
 */

/**
 * Sends the first meta byte and the next identifier byte.
 *  bytes: [ META | ID ]
 *
 *  Meta byte:
 *  7 6 5 4 3 2 1 0
 *  7:5 = Start bit
 *  4:2 = Data type
 *  1:0 = Data payload length
 * @param identifier
 * @param type      Message type
 * @param dataBytesLength
 */
void sendMeta(uint8_t identifier, uint8_t type, uint8_t dataBytesLength) {
    if (OPERATION_MODE == OPERATION_MODE_UNCONNECTED && type != SERIAL_DATA_TYPE_UPDATE_REQUEST) {
        return;
    }

#if !DEMO_SERIAL_CONNECTION
    Serial.write((SERIAL_META_START_BITS << 5U) | ((type & 0x07U) << 2U) | (dataBytesLength & 0x03U));
    Serial.write(identifier);
#endif
}

/**
 * Send a single toggle update:
 *  bytes: [ META | ID ]
 * @param identifier
 */
void sendToggleValueUpdate(const uint8_t identifier) {
#if !DEMO_SERIAL_CONNECTION
    sendMeta(identifier, SERIAL_DATA_TYPE_TOGGLE, 0);
#endif
    showButtonStatus(getControlNameForIdentifier(identifier));
}

/**
 * Send the value of a switch
 *  bytes: [ META | ID | STATE ]
 * @param identifier
 * @param value
 */
void sendSwitchValueUpdate(const uint8_t identifier, const uint8_t value) {
#if !DEMO_SERIAL_CONNECTION
    sendMeta(identifier, SERIAL_DATA_TYPE_BOOLEAN, 1);
    serialWriteBytes(value, 1);
#endif
    showSwitchStatus(getControlNameForIdentifier(identifier), value);
}

//void sendSliderValue(const uint8_t identifier, const int32_t value, const uint8_t length, const int8_t percentage) {
//#if !DEMO_SERIAL_CONNECTION
//    sendMeta(identifier, SERIAL_DATA_TYPE_TOGGLE, 0);
//    serialWriteBytes(value, length);
//#endif
//    showSliderStatus(getControlNameForIdentifier(identifier), percentage);
//}

/**
 * Send the aboslute (unsigned) value of something like a slider
 *  bytes: [ META | ID | (... | VALUE1 |) VALUE0 ]
 * @param identifier
 * @param value
 * @param length
 */
void sendAbsoluteValue(const uint8_t identifier, const uint32_t value, const uint8_t length) {
#if !DEMO_SERIAL_CONNECTION
    sendMeta(identifier, SERIAL_DATA_TYPE_ABSOLUTE_VALUE, length);
    serialWriteBytes(value, length);
#endif
}

/**
 * Send the relative (signed) value of something like a slider
 *  bytes: [ META | ID | (... | VALUE1 |) VALUE0 ]
 * @param identifier
 * @param value
 * @param length
 */
void sendRelativeValue(const uint8_t identifier, const int32_t value, const uint8_t length) {
#if !DEMO_SERIAL_CONNECTION
    sendMeta(identifier, SERIAL_DATA_TYPE_RELATIVE_VALUE, length);
    serialWriteBytes(value, length);
#endif
}

/**
 * Send the bytes in value
 * @param value :Word containing the value in the first byte(s)
 * @param length :Amount of bytes to send
 */
void serialWriteBytes(const int32_t value, uint8_t length) {
    if (OPERATION_MODE == OPERATION_MODE_UNCONNECTED) {
        return;
    }

#if !DEMO_SERIAL_CONNECTION
    while (length > 0) {
        Serial.write(value >> --length * 8);
    }
#endif
}