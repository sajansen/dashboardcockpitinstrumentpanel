//
// Created by Samuel on 26-8-2019.
//

#ifndef SERIALTOKEYBOARDSTROKESDEVICE_UTILS_H
#define SERIALTOKEYBOARDSTROKESDEVICE_UTILS_H

#include <Arduino.h>
#include "identifiers.h"
#include "screen.h"

void serialFlush();
void digitalPulse(uint8_t pin, uint8_t pulseDirection);
const char * memStr(const __FlashStringHelper *ifsh);

void sendMeta(uint8_t identifier, uint8_t type, uint8_t dataBytesLength);
void sendToggleValueUpdate(uint8_t identifier);
void sendSwitchValueUpdate(uint8_t identifier, uint8_t value);
//void sendSliderValue(uint8_t identifier, int32_t value, uint8_t length, int8_t percentage);
void sendAbsoluteValue(uint8_t identifier, uint32_t value, uint8_t length);
void sendRelativeValue(uint8_t identifier, int32_t value, uint8_t length);
void serialWriteBytes(int32_t value, uint8_t length);


#endif //SERIALTOKEYBOARDSTROKESDEVICE_UTILS_H
